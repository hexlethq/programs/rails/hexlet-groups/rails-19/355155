# Тестирование в Ruby

## test/stack_test.rb

Напишите тесты для Stack.

```ruby
stack = Stack.new
stack.to_a # []
stack.empty? # true
stack.size # 0

stack.push! 'ruby'
stack.push! 'php'
stack.push! 'java'
stack.to_a # ['ruby', 'php', 'java']
stack.size # 3
stack.empty? # false

stack.pop!
stack.to_a # ['ruby', 'php']
stack.size # 2

stack.clear!
stack.to_a # []
stack.empty? # true
```

### Подсказки

* [Minitest](https://github.com/seattlerb/minitest)
* [Stack](https://ru.wikipedia.org/wiki/%D0%A1%D1%82%D0%B5%D0%BA)
