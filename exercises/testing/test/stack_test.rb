# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stack = Stack.new %w[some data]
  end

  def test_stack_pop
    expected = %w[some]
    popped_variable = @stack.pop!
    assert_equal expected, @stack.to_a
    assert_equal popped_variable, 'data'
  end

  def test_stack_push
    expected = %w[some data another data]
    @stack.push!('another')
    @stack.push!('data')
    assert_equal expected, @stack.to_a
  end

  def test_stack_clear
    assert_equal 2, @stack.size
    @stack.clear!
    assert_equal 0, @stack.size
    assert_equal @stack.empty?, true
  end

  # END
end

# test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
# raise 'StackTest has not tests!' if test_methods.empty?
