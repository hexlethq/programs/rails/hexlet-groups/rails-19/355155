# frozen_string_literal: true

require_relative '../test_helper'

class BulletinsControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get root_path

    assert_match 'Home#index', @response.body
    assert_response :success
  end

  test 'should show bulletins' do
    get bulletins_path bulletins
    assert_match 'Bulletins', @response.body
    assert_response :success
  end

  test 'should show published bulletin' do
    published_bulletin = bulletins(:published)
    get bulletin_path published_bulletin
    assert_match published_bulletin.title, @response.body
    assert_match published_bulletin.body, @response.body
    assert_match 'is published', @response.body
    assert_response :success
  end

  test 'should show unpublished bulletin' do
    published_bulletin = bulletins(:unpublished)
    get bulletin_path published_bulletin
    assert_match published_bulletin.title, @response.body
    assert_match published_bulletin.body, @response.body
    assert_no_match 'is published', @response.body
    assert_response :success
  end
end
