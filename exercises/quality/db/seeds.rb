10.times do |index|
  Bulletin.create(title: "Title_#{index}", body: "Description_#{index}", published: (index % 2).odd?)
end
