# frozen_string_literal: true
class Task < ApplicationRecord
  validates :name, :status, :creator, presence: true
  validates :status, inclusion: { in: %w[new in_progress testing done], message: '%{value} is not a valid size' }
end
