100.times do
  Task.create(
    name: Faker::Name.name,
    description: Faker::Lorem.paragraphs.join(' '),
    status: 'new',
    creator: Faker::Name.name,
    performer: Faker::Name.name,
    completed: Faker::Boolean.boolean
  )
end
