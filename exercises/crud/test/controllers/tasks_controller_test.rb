require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  test 'tasks#index' do
    get tasks_path
    assert_response :success
    assert_match "Tasks", @response.body
  end

  test 'tasks#show' do
    task = tasks(:one)
    get task_path(task)

    assert_match "Task:", @response.body
    assert_response :success
  end

  test 'tasks#new' do
    get new_task_path

    assert_match "New Task", @response.body
    assert_response :success
  end

  test 'tasks#create' do
    attrs = {
      name: 'dump task',
      description: 'decs',
      status: 'new',
      creator: 'John',
      performer: 'Doe',
      completed: false
    }

    post tasks_path({ task: attrs })

    task = Task.find_by(name: attrs[:name])

    assert task
    assert_redirected_to task_path(task)
  end

  test 'tasks#edit' do
    task = tasks(:one)
    get edit_task_path(task)

    assert_response :success
    assert_match "Edit Task", @response.body
  end

  test 'tasks#update' do
    old_task = tasks(:one)
    attrs = {
      name: 'new name'
    }

    patch task_path(old_task), params: { task: attrs }

    task = Task.find(old_task[:id])

    assert task
    assert_redirected_to task_path(task)
    assert_equal task.name, attrs[:name]
  end

  test 'tasks#destroy' do
    task = tasks(:one)

    delete task_path(task)

    assert_redirected_to tasks_path
    assert_not_equal tasks.count, Task.count
  end
end
