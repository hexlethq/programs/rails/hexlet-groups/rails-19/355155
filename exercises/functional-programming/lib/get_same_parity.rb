# frozen_string_literal: true

# BEGIN
def get_same_parity(array)
  array.filter do |i|
    array.first.even? ? i.even? : i.odd?
  end
end

# END
