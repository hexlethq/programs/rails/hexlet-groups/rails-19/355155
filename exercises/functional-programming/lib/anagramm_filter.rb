# frozen_string_literal: true

# BEGIN
def anagramm_filter(base, words)
  words.filter do |word|
    base.chars.sort == word.chars.sort
  end
end

# END
