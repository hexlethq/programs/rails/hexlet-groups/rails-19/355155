# frozen_string_literal: true

# BEGIN
def fibonacci(num)
  if num <= 0
    nil
  elsif num == 1
    0
  elsif num == 2
    1
  else
    fibonacci(num - 2) + fibonacci(num - 1)
  end
end
# END
