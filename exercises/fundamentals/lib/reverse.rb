# frozen_string_literal: true

# BEGIN
def reverse(string)
  reversed_string = ''
  string.each_char do |c|
    reversed_string = "#{c}#{reversed_string}"
    # Изначальное решение было:
    # reversed_string.prepend c
    # но, насколько я понял, из-за frozen_string_literal: true строки мутировать нельзя.
    # Чем обусловленна данное ограничение?
  end
  reversed_string
end
# END
