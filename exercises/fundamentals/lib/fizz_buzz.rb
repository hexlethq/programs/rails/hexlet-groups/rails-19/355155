# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  sequence = []
  start.upto(stop) do |i|
    result = ''
    result += 'Fizz' if (i % 3).zero?
    result += 'Buzz' if (i % 5).zero?
    result = i if result.empty?
    sequence.push(result)
  end
  sequence.join ' '
end

# END
