class ExecutionTimer
  def initialize(app)
    @app = app
  end

  def call(env)
    time_start = Time.now
    status, header, body = @app.call(env)
    time_finish = Time.now

    [status, header, "#{body}!\n#{(time_finish - time_start) * 1000}"]
  end
end
