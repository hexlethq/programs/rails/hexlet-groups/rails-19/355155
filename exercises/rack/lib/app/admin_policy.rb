class AdminPolicy
  def initialize(app)
    @app = app
  end

  def call(env)
    status, header, body = @app.call(env)
    request = Rack::Request.new(env)

    return [403, {}, []] if request.path.start_with? '/admin'

    [status, header, body]
  end
end
