class Signature
  def initialize(app)
    @app = app
  end

  def call(env)
    status, header, body = @app.call(env)
    body_sha = Digest::SHA2.new(256).hexdigest(body)

    [status, header, "#{body}!\n#{body_sha}"]
  end
end
