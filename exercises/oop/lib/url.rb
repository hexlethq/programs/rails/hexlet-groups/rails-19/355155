# frozen_string_literal: true

# BEGIN

require 'uri'
require 'forwardable'

class Url
  extend Forwardable
  include Comparable

  def_delegators :@uri, :scheme, :host

  def initialize(url)
    @uri = URI(url)
  end

  def query_params
    hash = @uri.query.split('&').inject({}) do |result, item|
      key, value = item.split('=')
      result.merge({ key => value })
    end
    hash.transform_keys(&:to_sym)
  end

  def query_param(param, default = nil)
    query_params.fetch(param, default)
  end

  def to_s
    @uri.to_s
  end

  def <=>(other)
    to_s <=> other.to_s
  end
end

# END
