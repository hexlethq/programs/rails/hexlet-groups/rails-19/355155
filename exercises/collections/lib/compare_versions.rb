# frozen_string_literal: true

# BEGIN
def compare_versions(first_ver, second_ver)
  first_major_ver, first_minor_ver = first_ver.split('.').map(&:to_i)
  second_major_ver, second_minor_ver = second_ver.split('.').map(&:to_i)

  return first_major_ver <=> second_major_ver unless first_major_ver == second_major_ver

  first_minor_ver <=> second_minor_ver
end

# END
