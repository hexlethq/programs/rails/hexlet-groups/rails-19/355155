# frozen_string_literal: true

# BEGIN
def build_query_string(query_params)
  query_params.sort
              .map { |item| item.join('=') }
              .join('&')
end
# END
