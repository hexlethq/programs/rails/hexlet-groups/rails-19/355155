# frozen_string_literal: true

# BEGIN
module Model
  attr_accessor :attributes

  def self.included(klass)
    klass.extend(ClassMethods)
  end

  module ClassMethods
    attr_reader :schema

    def cast_to(attribute, value)
      return nil if value.nil?

      cast_to_class = {
        integer: ->(cast_value) { cast_value.to_i },
        string: ->(cast_value) { cast_value.to_s },
        datetime: ->(cast_value) { DateTime.parse(cast_value) },
        boolean: ->(cast_value) { !!cast_value }
      }

      cast_to_class[attribute].call(value)
    end

    def attribute(name, options = {})
      @schema ||= {}
      @schema[name] = options

      define_method name do
        attributes[name]
      end

      define_method "#{name}=" do |value|
        attributes[name] = self.class.cast_to(options[:type], value)
      end
    end
  end

  def initialize(attributes = {})
    @attributes = self.class.schema.each_with_object({}) do |(key, options), result|
      value = attributes.key?(key) ? attributes[key] : options[:default]
      result[key] = self.class.cast_to(options[:type], value)
    end
  end
end
# END
